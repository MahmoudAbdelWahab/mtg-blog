<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/*
* class User model
**/
class User extends AppModel{

    //  name of model
    public $name = 'User';
    // name of table of User Model in db 
    public $useTable = 'users';

    // validation 
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A name is required'
            )
        ),
        'password' =>array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            ),
            'Match Passwords' =>array(
                'rule' => 'matchPasswords',
                'message' => 'Your passwords donُt match'
            )
        ),
        'email' => array(
             'Valid email' => array(
                'rule' => array('email'),
                'message' => 'Please enter a valid Email!'
            )
        ),
        'role' =>array(
            'valid' =>array(
                'rule' =>array('inList', array('admin','author')),
                'message' => 'Please enter the valid role!',
                'allowEmpty' => false
            )
        ),
        'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
    );

    var $hasMany = array(
        'Post' =>array(
            'className' => 'Post',
            'foreignKey' => 'user_id',

        )
    );


    // //   this is function for hash password before saving in db
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    // this is function for match two password in register form
    public function matchPasswords($data){
        if($data['password'] == $this->data['User']['password_confirmation']){
            return true;
        }
       
        $this->invalidate('password_confirmation','Your passwords donُt match');
        return false;
    }

}