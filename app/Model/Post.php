<?php

App::uses('AppModel', 'Model');

/*
* Class Post Model
**/
class Post extends AppModel{

    // vaidation
    public $validate = array(
        'title' => array(
            'rule' => 'notBlank'
        ),
        'content' => array(
            'rule' => 'notBlank'
        )
    );

    
    var $belongsTo = array(
        'User' =>array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );


    var $hasMany = array(
        'Comment' =>array(
            'className' => 'Comment',
            'foreignKey' => 'post_id',

        )
    );
}