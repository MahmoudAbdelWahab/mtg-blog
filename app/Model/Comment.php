<?php

App::uses('AppModel', 'Model');

/*
* Class Comment Model
**/
class Comment extends AppModel{

    // vaidation
    public $validate = array(
        'content' => array(
            'rule' => 'notBlank'
        )
    );

    
    var $belongsTo = 'Post';

}