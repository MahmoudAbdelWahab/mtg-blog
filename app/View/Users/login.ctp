<!-- start div of users form login  -->
<div class="users form">
    <?php echo $this->Session->flash('auth'); ?>
    <?php echo $this->Form->create('User'); ?>
        <fieldset>
            <legend>Login</legend>
            <?php 
                echo $this->Form->input('username',array(
                    'div' => array(
                        'style' =>'font-weight:bold'
                    )
                ));
                echo $this->Form->input('password',array(
                    'div' => array(
                        'style' =>'font-weight:bold',
                    )
                ));
            ?>
        </fieldset>
    <?php echo $this->Form->end(__('Login')); ?>
</div>
<!-- end of div  -->