<!-- start div of  users edit  -->
<div class="users form">
    <?php echo $this->Form->create('User');?>
        <fieldset>
            <legend>Edit</legend>
            <?php 
                echo $this->Form->input('username',array(
                    'div' => array(
                        'style' =>'font-weight:bold'
                    )
                ));
                echo $this->Form->input('email',array(
                    'div' => array(
                        'style' =>'font-weight:bold',
                    )
                ));
                echo $this->Form->input('role', array(
                'options' => array('admin' => 'Admin', 'author' => 'Author')
              ));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Submit'); ?>
</div>
<!-- end of div  -->

<!-- start div action  -->
<div class='actions'>
    <h3>Actions</h3>
    <ul>
        <li><?php echo $this->Html->link('List Users', array('action' => 'index'));?></li>
    </ul>
</div>
<!-- end div  -->