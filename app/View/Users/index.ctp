<!-- start of div users index  -->
<div class="users form">
    <h2>Users</h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>id</th>
            <th>name</th>
            <th>email</th>
            <th>role</th>
            <th class="actions">Actions</th>
        </tr>

        <?php
        $i =0;
        foreach($users as $user):
            $class = null;
            if($i++ % 2 == 0){
                $class = ' class="altrow"';
            }
        ?>

        <tr <?php echo $class;?>>
            <td><?php echo $user['User']['id']; ?>&nbsp;</td>
            <td><?php echo $user['User']['username']; ?>&nbsp;</td>
            <td><?php echo $user['User']['email']; ?>&nbsp;</td>
            <td><?php echo $user['User']['role']; ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Html->link('View',array('action'=>'view',$user['User']['id'])); ?>
                <?php if($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'):?>
                    <?php echo $this->Html->link('Edit',array('action'=>'edit',$user['User']['id']));?>
                    <?php echo $this->Form->postLink('Delete',
                    array('action'=>'delete',$user['User']['id']),
                    array('confirm' => 'Are You sure you want to delete This User?')
                    );?>
                <?php endif;?>
            </td>
        </tr>
        <?php endforeach;?>
    </table>
</div>
<!-- end of div  -->

<!-- start of div action  -->
<div class="actions">
    <h3><?php echo('Actions');?></h3>
    <ul>
        <li> <?php echo $this->Html->Link('New User', array('action' => 'add'));?> </li>
        <li> <?php echo $this->Html->Link('List posts', array('controller'=>'posts','action' => 'index'));?> </li>
    </ul>
</div>
<!-- end of div  -->