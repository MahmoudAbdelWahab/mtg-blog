<!-- view data of one user  -->
<!--first of div users view  -->
<div class="users form">
    <h2>User</h2>
        <dl <?php $i = 0; $class = ' class="altrow"';?>>
            <dt <?php if($i % 2 == 0) echo $class;?>>Id</dt>
            <dd <?php if($i++ % 2 == 0) echo $class;?>>
                <?php echo $user['User']['id'];?>
                &nbsp;
            </dd>

            <dt <?php if($i % 2 == 0) echo $class;?>>Name</dt>
            <dd <?php if($i++ % 2 == 0) echo $class;?>>
                <?php echo $user['User']['username'];?>
                &nbsp;
            </dd>

            <dt <?php if($i % 2 == 0) echo $class;?>>Email</dt>
            <dd <?php if($i++ % 2 == 0) echo $class;?>>
                <?php echo $user['User']['email'];?>
                &nbsp;
            </dd>
        </dl>
</div>
<!-- end of div  -->

<!-- aside for action"create - edit - delete - List " of users!  -->
<!-- statrt of div actions  -->
<div class="actions">
    <h3><?php echo "Actions";?></h3>
    <ul>
        <?php if($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'):?>
            <li> <?php echo $this->Html->Link('Edit User', array('action' => 'edit', $user['User']['id']));?> </li>
            <li> <?php echo $this->Form->postLink('Delete User', array('action' => 'delete', $user['User']['id']),
                    array('confirm' => 'Are You sure you want to delete This User?'));?> </li>
        <?php endif;?>
        <li> <?php echo $this->Html->Link('List Users', array('action' => 'index'));?> </li>
        <li> <?php echo $this->Html->Link('New User', array('action' => 'add'));?> </li>
    </ul>
</div>
<!-- end of div actions  -->