<div class="posts index">
<h2>Posts</h2>
<table cellpadding="0" cellspacing="0">
    <tr>
        <th>id</th>
        <th>title</th>
        <th>content</th>
        <th class="actions">Actions</th>
    </tr>

    <?php
    $i =0;
    foreach($posts as $post):
        $class = null;
        if($i++ % 2 == 0){
            $class = ' class="altrow"';
        }
    ?>
    
    <tr <?php echo $class;?>>
        <td><?php echo $post['Post']['id']; ?>&nbsp;</td>
        <td><?php echo $post['Post']['title']; ?>&nbsp;</td>
        <td><?php echo $post['Post']['content']; ?>&nbsp;</td>
        <td class="actions">
            <?php echo $this->Html->link('View',array('action'=>'view',$post['Post']['id'])); ?>
            <?php if($current_user['id'] == $post['Post']['user_id'] || $current_user['role'] == 'admin'):?>
                <?php echo $this->Html->link('Edit',array('action'=>'edit',$post['Post']['id']));?>
                <?php echo $this->Form->postLink(
                    'Delete',
                    array('action' =>'delete',$post['Post']['id']),
                    array('confirm' => 'Are you sure you want to delete this Post?')
                    );
                ?>
            <?php endif;?>
        </td>
    </tr>
    <?php endforeach;?>
</table>
</div>

<div class="actions">
    <h3><?php echo('Actions');?></h3>
    <ul>
    <?php if($current_user['role'] == 'author'):?>
         <li> <?php echo $this->Html->Link('New Post', array('action' => 'add',$user_id));?> </li> 
    <?php endif;?>  
        <li> <?php echo $this->Html->Link('List Users', array('controller'=>'users','action' => 'index'));?> </li>
        <li> <?php echo $this->Html->Link('List Comments', array('controller'=>'comments','action' => 'index'));?> </li>
    </ul>
</div>