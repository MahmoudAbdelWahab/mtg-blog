<div class="posts view">
<h2>View Post</h2>
    <dl <?php $i = 0; $class = ' class="altrow"';?>>
        <dt <?php if($i % 2 == 0) echo $class;?>>Id</dt>
        <dd <?php if($i++ % 2 == 0) echo $class;?>>
            <?php echo $post['Post']['id'];?>
            &nbsp;
        </dd>

        <dt <?php if($i % 2 == 0) echo $class;?>>Title</dt>
        <dd <?php if($i++ % 2 == 0) echo $class;?>>
            <?php echo $post['Post']['title'];?>
            &nbsp;
        </dd>

        <dt <?php if($i % 2 == 0) echo $class;?>>Content</dt>
        <dd <?php if($i++ % 2 == 0) echo $class;?>>
            <?php echo $post['Post']['content'];?>
            &nbsp;
        </dd>
    </dl>
</div>

<div class="actions">
    <h3><?php echo "Actions";?></h3>
    <ul>
    <?php if($current_user['id'] == $post['Post']['user_id'] || $current_user['role'] == 'admin'):?>
        <li> <?php echo $this->Html->Link('Edit Post', array('action' => 'edit', $post['Post']['id']));?> </li>
        <li> <?php echo $this->Form->postLink('Delete Post',
         array('action' => 'delete', $post['Post']['id']),
         array('confirm'=> 'Are you sure you want to delete this Post?')
        );
        ?> 
    <?php endif; ?>
        </li>
        <li> <?php echo $this->Html->Link('List Posts', array('action' => 'index'));?> </li>
        <li> <?php echo $this->Html->Link('New Post', array('action' => 'add'));?> </li>
        <li> <?php echo $this->Html->Link('List Comments', array('action' => 'index'));?> </li>
        <li> <?php echo $this->Html->Link('New Comment', array('action' => 'add'));?> </li>
    </ul>
</div>