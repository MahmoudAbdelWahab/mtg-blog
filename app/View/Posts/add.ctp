<div class="posts form">
    <?php echo $this->Form->create('Post');?>
        <fieldset>
            <legend>Add Post</legend>
            <?php 
                echo $this->Form->input('title',array(
                    'div' => array(
                        'style' =>'font-weight:bold'
                    )
                ));
                echo $this->Form->input('content',array(
                    'div' => array(
                        'style' =>'font-weight:bold',
                    ),
                    'type' => 'textarea'
                ));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Add'); ?>
</div>
<div class='actions'>
    <h3>Actions</h3>
    <ul>
        <li><?php echo $this->Html->link('List Posts', array('action' => 'index'));?></li>
    </ul>
</div>