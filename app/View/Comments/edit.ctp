<div class="comments form">
    <?php echo $this->Form->create('Comment');?>
        <fieldset>
            <legend>Edit Comment</legend>
            <?php 
                echo $this->Form->input('content',array(
                    'div' => array(
                        'style' =>'font-weight:bold',
                    ),
                    'type' => 'textarea'
                ));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Save Post'); ?>
</div>
<div class='actions'>
    <h3>Actions</h3>
    <ul>
        <li><?php echo $this->Html->link('List Posts', array('action' => 'index'));?></li>
    </ul>
</div>