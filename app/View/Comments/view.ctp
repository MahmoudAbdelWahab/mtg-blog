<div class="comments view">
<h2>View Comment</h2>
    <dl <?php $i = 0; $class = ' class="altrow"';?>>
        <dt <?php if($i % 2 == 0) echo $class;?>>Id</dt>
        <dd <?php if($i++ % 2 == 0) echo $class;?>>
            <?php echo $comment['Comment']['id'];?>
            &nbsp;
        </dd>

        <dt <?php if($i % 2 == 0) echo $class;?>>Content</dt>
        <dd <?php if($i++ % 2 == 0) echo $class;?>>
            <?php echo $comment['Comment']['content'];?>
            &nbsp;
        </dd>
    </dl>
</div>

<div class="actions">
    <h3><?php echo "Actions";?></h3>
    <ul>
    <?php if($current_user['id'] == $comment['Post']['user_id'] || $current_user['role'] == 'admin'):?>
        <li> <?php echo $this->Html->Link('Edit Comment', array('action' => 'edit', $comment['Comment']['id']));?> </li>
        <li> <?php echo $this->Form->postLink('Delete Comment',
         array('action' => 'delete', $comment['Comment']['id']),
         array('confirm'=> 'Are you sure you want to delete this Post?')
        );
        ?> 
    <?php endif; ?>
        </li>
        <li> <?php echo $this->Html->Link('List Comments', array('action' => 'index'));?> </li>
        <li> <?php echo $this->Html->Link('New Comment', array('action' => 'add'));?> </li>
    </ul>
</div>