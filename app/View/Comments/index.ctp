<div class="comments index">
<h2>Comments</h2>
<table cellpadding="0" cellspacing="0">
    <tr>
        <th>Post title</th>
        <th>id</th>
        <th>Comment</th>
        <th class="actions">Actions</th>
    </tr>

    <?php
    $i =0;
    foreach($comments as $comment):
        $class = null;
        if($i++ % 2 == 0){
            $class = ' class="altrow"';
        }
    ?>

    <tr <?php echo $class;?>>
        <td><?php echo $this->Html->link($comment['Post']['title'],array('controller'=>'posts','action'=>'view',$comment['Post']['id'])); ?>&nbsp;</td>
        <td><?php echo $comment['Comment']['id']; ?>&nbsp;</td>
        <td><?php echo $comment['Comment']['content']; ?>&nbsp;</td>
        <td class="actions">
            <?php echo $this->Html->link('View',array('action'=>'view',$comment['Comment']['id'])); ?>
            <?php if($current_user['id'] == $comment['Post']['user_id'] || $current_user['role'] == 'admin'):?>
                <?php echo $this->Html->link('Edit',array('action'=>'edit',$comment['Comment']['id']));?>
                <?php echo $this->Form->postLink(
                    'Delete',
                    array('action' =>'delete',$comment['Comment']['id']),
                    array('confirm' => 'Are you sure you want to delete this Post?')
                    );
                ?>
            <?php endif;?>
        </td>
    </tr>
    <?php endforeach;?>
</table>
</div>

<div class="actions">
    <h3><?php echo('Actions');?></h3>
    <ul>
    <?php if($current_user['role'] == 'author'):?>
         <li> <?php echo $this->Html->Link('New Comment', array('action' => 'add',$post['Post']['id']));?> </li>
    <?php endif;?>     
        <li> <?php echo $this->Html->Link('List posts', array('controller'=>'posts','action' => 'index'));?> </li>
    </ul>
</div>