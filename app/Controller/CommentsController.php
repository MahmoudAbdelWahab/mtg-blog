<?php

App::uses('AppController','Controller');


/*
* Class CommentsController 
**/
class CommentsController extends AppController{

    public $helpers = array('Html','Form');
    public $components = array('Flash');

    // index function to view all comments in db
    public function index(){
        // echo "<pre>";
        // var_dump();
        // die;
        $this->set('post',$this->Comment->Post->find('first'));
        $this->set('comments',$this->Comment->find('all'));
    }

    // // view function for view comment by id
    public function view($id = NULL){
        if(!$id){
            throw new NotFoundException(__('Invalid Post'));
        }

        $comment = $this->Comment->findById($id);
        if(!$comment){
            throw new NotFoundException(__('Invalid Post'));
        }
        $this->set('comment',$comment);
    }

    // // function add to add new comment in db
    public function add($postid = null){

        if(!empty($this->data)){
            $this->Comment->create();
            $this->request->data['Comment']['post_id'] = $postid;
            if($this->Comment->save($this->request->data)){
                $this->Flash->success(__('Your Comment has been Saved!'));
                return $this->redirect(array('action'=>'index'));
            }else{
                $this->Session->setFlash(__('The Comment could not be saved. Please, 
                                          try again.', true));
            }
        }
    }

    // // function edit for edit Comment by id
    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        $comment = $this->Comment->findById($id);
        if (!$comment) {
            throw new NotFoundException(__('Invalid comment'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Comment->id = $id;
            if ($this->Comment->save($this->request->data)) {
                $this->Flash->success(__('Your comment has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your comment.'));
        }

        if (!$this->request->data) {
            $this->request->data = $comment;
        }
    }

    // // function delete for delete any comment by id
    public function delete($id) {

        $this->request->allowMethod('post');

        $this->Comment->id = $id;
            
        if (!$this->Comment->exists()) {
            throw new NotFoundException(__('Invalid Comment'));
        }
            
        if ($this->Comment->delete()) {
            $this->Flash->success(__('Comment deleted'));
            return $this->redirect(array('action' => 'index'));
        }

        $this->Flash->error(__('Comment was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

}