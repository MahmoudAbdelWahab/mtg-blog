<?php

App::uses('AppController','Controller');
App::import('Model','User');

/*
* Class PostsController 
**/
class PostsController extends AppController{

    public $helpers = array('Html','Form');
    public $components = array('Flash');

    // index function to view all post in db
    public function index(){
       
        $this->set('user_id',$this->Auth->user('id'));
        $this->set('posts',$this->Post->find('all'));
    }

    // view function for view one post by id
    public function view($id = NULL){
        if(!$id){
            throw new NotFoundException(__('Invalid Post'));
        }

        $post = $this->Post->findById($id);
        if(!$post){
            throw new NotFoundException(__('Invalid Post'));
        }
        $this->set('post',$post);
    }

    // function add to add new post in db
    public function add($userid = null){
        
        if(!empty($this->data)){
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $userid;
            if($this->Post->save($this->request->data)){
                $this->Flash->success(__('Your Post has been Saved!'));
                return $this->redirect(array('action'=>'index'));
            }else{
                $this->Flash->error(__('The Post could not be saved. Please, 
                                          try again.', true));
            }
        }
    }

    // function edit for edit post by id
    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }

    // function delete for delete any post by id
    public function delete($id) {

        $this->request->allowMethod('post');

        $this->User->id = $id;
            
        if (!$this->Post->exists()) {
            throw new NotFoundException(__('Invalid Post'));
        }
            
        if ($this->Post->delete()) {
            $this->Flash->success(__('Post deleted'));
            return $this->redirect(array('action' => 'index'));
        }

        $this->Flash->error(__('Post was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

}