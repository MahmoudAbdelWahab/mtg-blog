<?php

App::uses('AppController', 'Controller');

/*
* class UserController 
*
*/
class UsersController extends AppController{

    public $name = 'Users';

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register.
        $this->Auth->allow('add','logout');
    }

    public function isAuthorized($user){
        if($user['role'] == 'admin'){
            return true;
        }

        if(in_array($this->Action,array('edit','delete'))){
            if($user['id'] != $this->request->params['pass'][0]){
                return false;
            }
            return true;
        }
    }

    // // this is login function
    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirectUrl());
            }else{
                $this->Flash->error(__('Username or password is incorrect'));
                $this->redirect(array('action'=>'login'));
        }
        }
    }

    // // this is logout function
    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    // index function to view all users founded
    public function index(){
        $this->User->recursive = 0 ;
        $this->set('users',$this->User->find('all'));
    }
    // view function to view one user by id
    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if(!$id){
            $this->Flash->error(__('Invalid User'));
        }
        $this->set('user', $this->User->findById($id));
    }

    // add function to add new user
    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            // $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
    }

    // edit function for edit one user by id 
    public function edit($id = null) {
        
        $this->User->id = $id;
        
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        
        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been Edited'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    // delete function for delete one user by id
    public function delete($id = null) {        
        $this->request->allowMethod('post');

        $this->User->id = $id;
        
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        
        if ($this->User->delete()) {
            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }

        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

}